const mongoose = require(`mongoose`); //importo el modulo de mongoose que es el orm
console.log(process.env.MONGODB_URI); //esto me imprime la variable de coneccion del entorno
const URI = process.env.MONGODB_URI
  ? process.env.MONGODB_URI
  : `mongodb://localhost/challenge`; // esta const la uso para no hacer tan larga la coneccion y en mongo no hace falta crear la base
// aca inicializo la coneccion
mongoose.connect(URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});

// guardo la propiedad para escuchar la coneccion
const connection = mongoose.connection;

connection.once(`open`, () => {
  console.log("DB is conected");
});
