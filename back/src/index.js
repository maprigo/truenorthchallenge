require(`dotenv`).config(); //importr las variables de entorno
const app = require(`./app`); // importo mi app que lo estoy exportando de app.js
require("./db"); //como este archivo no exporta nada no lo almaceno simplemente ejecuta codigo
// uso una function asi pueo usar await , y no usar callbacks
async function main() {
  await app.listen(app.get(`port`)); // iniciar el servidor en el 4000
  console.log(`Server on port ${app.get(`port`)}`);
}

main(); // invoco la function main