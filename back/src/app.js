const express = require(`express`); // nos permite crear un servidor
const cors = require("cors");
const app = express(); //esto te devuelve un objeto que es nuestro server

// settings
app.set(`port`, process.env.PORT || 4000); // esto se hace para mantener una raiz de modif

// middleware
app.use(cors()); //esto lo invoco para que lo use y no tener problemas de cors
app.use(express.json()); //para que mi aplicacion pueda interpretar json

//routes le pido que importe el archivo correspondiente para cada route
app.use("/api/tasks", require("./routes/tasks"));

module.exports = app; //exporto app por que app.js solo define el server no lo inicializa
