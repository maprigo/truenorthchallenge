const { Router } = require("express"); //importo la funcion router del modulo express
const router = Router();

const {
  getTasks,
  createTask,
  getTask,
  deleteTask,
  updateTask,
  takeData,
  doneTask,
} = require("../controllers/tasks.controller");

// cada vez que visiten la raiz api tasks


router.route("/").get(takeData).put(doneTask);
router.route("/dashboard").get(getTasks).post(createTask);
router.route("/:id").get(getTask).put(doneTask);
router.route("/dashboard/:id").get(getTask).delete(deleteTask).put(updateTask);

module.exports = router;
