// ESTO LO USO PARA PODER GENERAR LOS SCHEMAS  COMO SI FUESE MYSQL
const { Schema, model } = require("mongoose");

const taskSchema = new Schema(
  {
    title: String,
    content: String,
    done: Boolean,
  },
  {
    timestamps: true, //este dato va a sumar dos datos datos de actualizacion y de creacion
  }
);
// create name of model and use task schema
module.exports = model("Task", taskSchema);
