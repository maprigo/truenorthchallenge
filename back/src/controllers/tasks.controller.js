const fetch = require('node-fetch');

//empty const
const tasksCtrl = {};

// Import model of models folder
const Task = require("../models/Task");

const rand = (min, max) =>{
  if (min==null && max==null)
    return 0;    
  
  if (max == null) {
      max = min;
      min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
  };


tasksCtrl.takeData = async (req, res) => {
  
  let {cant=3} = req.body;
  let response = await fetch('https://hipsum.co/api/?type=hipster-centric');
  let phrases = await response.json();
  console.log(phrases[0]);
  
  let word = phrases[0].split(" "); 
  let max = word.length;
  let min = 0;
  
  console.log(word.length);
  console.log(word[2]);

  
  for (let count = 1; count <= cant; count++) {
    let index = rand(min,max);
    const newTask = new Task({title:word[index],content:word[index]
    });
    // save new task on data base
    await newTask.save();
  }
  
  //   response of server like a mss
  res.json(`New ${cant} Task added`);
}

// all operations with db are asyncronic
tasksCtrl.getTasks = async (req, res) => {
  try {
    const tasks = await Task.find(); // return all tasks in array
    res.json(tasks);
    
  } catch (error) {
    console.log(error);
    
  }
   //response this tasks like  a json
};

tasksCtrl.createTask = async (req, res) => {
  const { title, content, author } = req.body; //take body of request  and take data like a const
  //create a const of newTask
  const newTask = new Task({
    title,
    content,
  });
  // save new task on data base
  await newTask.save();
  //   response of server like a msj
  res.json("New Task added");
};

tasksCtrl.getTask = async (req, res) => { 
  const task = await Task.findById(req.params.id); // take de task id y and search in DB
  res.json(task); //task -> return a list of a task with this id
};

tasksCtrl.deleteTask = async (req, res) => {
  await Task.findByIdAndDelete(req.params.id);
  res.json("Task Deleted");
};

tasksCtrl.doneTask = async (req, res) => {
  try {
    await Task.findByIdAndUpdate(req.params.id, {done:true});
    res.json("Task Done");
    
  } catch (error) {
    console.log(error);
  }
  
  
};

tasksCtrl.updateTask = async (req, res) => {
  const { title, content } = req.body;
  await Task.findByIdAndUpdate(req.params.id, {
    title,
    content,
  });
  res.json("Task Updated");
};

module.exports = tasksCtrl;
