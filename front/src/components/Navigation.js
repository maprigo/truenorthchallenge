import React, { Component } from "react";
import axios from 'axios'
import { Link } from "react-router-dom";
import {getTasks} from "./TasksList";

export default class Navigation extends Component {
  generateTasks = async () => {
    await axios.get('http://localhost:4000/api/tasks/');
    window.location.reload(true);
  }
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark p-3">
        <div className="container">
          <Link className="navbar-brand" to="/">
            <i className="material-icons">assignment </i> TrueNorth
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <Link to="/" className="nav-link">
                  Tasks
                </Link>
              </li>
              <li className="nav-item" onClick={() => this.generateTasks()}>
                <Link to="/" className="nav-link">
                  Generate Tasks
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/create" className="nav-link">
                  Create Task
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
