import React, { Component } from 'react'
import axios from 'axios'
import { format } from 'timeago.js'
import { Link } from 'react-router-dom'

export default class TasksList extends Component {

    state = {
        tasks: []
    }

    async componentDidMount() {
        this.getTasks();
    }

    getTasks = async () => {
        const res = await axios.get('http://localhost:4000/api/tasks/dashboard/')
        console.log(res.data);
        this.setState({
            tasks: res.data
        });
    }

    deleteTask = async (taskId) => {
        await axios.delete('http://localhost:4000/api/tasks/dashboard/' + taskId);
        this.getTasks();
    }
    doneTask = async (taskId) => {
        await axios.put('http://localhost:4000/api/tasks/' + taskId);
        this.getTasks();
    }

    render() {
        return (
            <div className="row">
                {
                    this.state.tasks.map(task => (
                        <div className="col-md-4 p-2" key={task._id}>
                            <div className="card">
                                <div className="card-header d-flex justify-content-between">
                                
                                    <h5>{task.title}</h5>
                                        
                                    
                                    <Link to={"/edit/" + task._id} className="btn btn-secondary">
                                        <i className="material-icons">
                                            border_color</i>
                                    </Link>
                                    { task.done 
                                        ? <i className="material-icons" style ={{ color: "green", fontSize: 40} } >
                                        check</i>
                                        :
                                        <button className="btn btn-success" onClick={() => this.doneTask(task._id)}>
                                        Done
                                        </button>
                                    }
                                    
                                </div>
                                <div className="card-body" >
                                    <p>
                                        Content:{task.content}
                                    </p>
                                    <p>
                                        {format(task.createdAt)}
                                    </p>
                                    <p>
                                        
                                    </p>
                                </div>
                                <div className="card-footer">
                                    <button className="btn btn-danger" onClick={() => this.deleteTask(task._id)}>
                                        Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        )
    }
}
