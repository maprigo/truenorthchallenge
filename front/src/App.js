import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css"; // boostrap import
import "./App.css";

// Imports Components
import Navigation from "./components/Navigation";
import TasksList from "./components/TasksList";
import CreateTask from "./components/CreateTask";

function App() {
  return (
    <Router>
      <Navigation />
      <Route exact path="/" component={TasksList} />
      <Route path="/edit/:id" component={CreateTask} />
      <Route path="/create" component={CreateTask} />
    </Router>
  );
}

export default App;
